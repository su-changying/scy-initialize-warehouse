# Generated by Django 2.2.2 on 2022-07-19 06:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20220719_0933'),
    ]

    operations = [
        migrations.AddField(
            model_name='usermodel',
            name='is_cativated',
            field=models.BooleanField(default=True, verbose_name='是否激活管理员权限'),
        ),
        migrations.AddField(
            model_name='usermodel',
            name='is_staff',
            field=models.BooleanField(default=False, verbose_name='是否是管理员'),
        ),
    ]
