# Generated by Django 2.2.2 on 2022-07-19 01:28

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UserModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=30, verbose_name='用户名')),
                ('phone', models.CharField(max_length=20, verbose_name='手机号')),
                ('password', models.CharField(max_length=300, verbose_name='密码')),
                ('create_time', models.DateTimeField(default=datetime.datetime(2022, 7, 19, 9, 28, 2, 562756), verbose_name='注册时间')),
                ('last_login', models.DateTimeField(default=datetime.datetime(2022, 7, 19, 9, 28, 2, 562756), verbose_name='最后登录时间')),
            ],
            options={
                'db_table': 'tb_user',
            },
        ),
    ]
