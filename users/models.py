from django.db import models
import datetime
from django.utils import timezone
# Create your models here.
class UserModel(models.Model):
    username = models.CharField(max_length=30,verbose_name='用户名')
    phone = models.CharField(max_length=20,verbose_name='手机号')
    password = models.CharField(max_length=300,verbose_name='密码')
    create_time = models.DateTimeField(verbose_name='注册时间',default=timezone.now)
    last_login = models.DateTimeField(verbose_name='最后登录时间',default=timezone.now)
    is_staff = models.BooleanField(verbose_name='是否是管理员',default=False)
    is_cativated = models.BooleanField(verbose_name='是否激活管理员权限',default=True)
    class Meta:
        # 定义mysql中的表明
        db_table = 'tb_user'
    def __str__(self):
        return self.username